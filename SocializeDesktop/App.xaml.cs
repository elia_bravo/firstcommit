﻿// Copyright 2016 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Windows;

namespace OAuthApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void App_SessionEnding(object sender, SessionEndingCancelEventArgs e)
        {
            
            var main = App.Current.MainWindow as MainWindow; // If not a static method, this.MainWindow would work
            main.EndSession();

            // Ask the user if they want to allow the session to end
            string msg = string.Format("{0}. Exit from FeelHabits?", e.ReasonSessionEnding);
            MessageBoxResult result = MessageBox.Show(msg, "Session Ending", MessageBoxButton.OK);

            // End session, if specified
            if (result == MessageBoxResult.OK)
            {
                e.Cancel = true;
                main.ResumeSession();

            }

        }
    }
}
