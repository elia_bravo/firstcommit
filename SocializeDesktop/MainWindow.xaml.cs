﻿// Copyright 2016 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Google.Cloud.Firestore;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Globalization;
using Microsoft.Win32;

namespace OAuthApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // client configuration
        const string clientID = "531289543247-pjuefhns5avkidfl7272o4mr3t6mj2dg.apps.googleusercontent.com";
        const string clientSecret = "snQqV87BB8iE18hIG9WHtlQG";
        const string authorizationEndpoint = "https://accounts.google.com/o/oauth2/auth";
        const string tokenEndpoint = "https://oauth2.googleapis.com/token";
        const string userInfoEndpoint = "https://www.googleapis.com/oauth2/v3/userinfo";

        /*keyboard and mouse events*/
        private DispatcherTimer timer;
        private AllInputSources lastInput;
        private KeyboardInput keyboard;
        private MouseInput mouse;
        private long lastSavedInput;
        private Boolean endSession = false;
        FirestoreDb db;
        String email;


        const string projectId = "socialize-d38e1";
        private bool mRequestClose;


        private bool RequestClose
        {
            get
            {
                return mRequestClose;
            }
            set
            {
                mRequestClose = value;
            }
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!RequestClose)
            {
                WindowState = WindowState.Minimized;
                e.Cancel = true;
                this.Hide();
            }
        }

        // Minimize to system tray when application is minimized.
        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == WindowState.Minimized) this.Hide();

            base.OnStateChanged(e);
        }

        private void Cms_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            switch (item.Name)
            {
                case "mnuOpen":
                    this.Show();
                    this.WindowState = WindowState.Normal;
                    this.BringIntoView();
                
                    break;
                    /*
                    case "mnuSettings":
                        System.Windows.MessageBox.Show("Settings Menu");
                        break;
                    case "mnuClose":
                        RequestClose = true;
                        Close();
                        break;
                    */
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            rk.SetValue("FeelHabitsDesktop", System.Windows.Forms.Application.ExecutablePath.ToString());

            db = FirestoreDb.Create(projectId);
            Console.WriteLine("Created Cloud Firestore client with project ID: {0}", projectId);


            /* ADD DATA
            DocumentReference docRef = db.Collection("users").Document("alovelace");
            Dictionary<string, object> user = new Dictionary<string, object>
{
            { "First", "Ada" },
            { "Last", "Lovelace" },
            { "Born", 1815 }
};
            await docRef.SetAsync(user);


            DocumentReference docRef = db.Collection("users").Document("aturing");
            Dictionary<string, object> user = new Dictionary<string, object>
            {
                { "First", "Alan" },
                { "Middle", "Mathison" },
                { "Last", "Turing" },
                { "Born", 1912 }
            };
            await docRef.SetAsync(user);
            */

            /*READ DATA
             * CollectionReference usersRef = db.Collection("users");
            QuerySnapshot snapshot = await usersRef.GetSnapshotAsync();
            foreach (DocumentSnapshot document in snapshot.Documents)
            {
                Console.WriteLine("User: {0}", document.Id);
                Dictionary<string, object> documentDictionary = document.ToDictionary();
                Console.WriteLine("First: {0}", documentDictionary["First"]);
                if (documentDictionary.ContainsKey("Middle"))
    {
                Console.WriteLine("Middle: {0}", documentDictionary["Middle"]);
            }
            Console.WriteLine("Last: {0}", documentDictionary["Last"]);
            Console.WriteLine("Born: {0}", documentDictionary["Born"]);
            Console.WriteLine();
        }
             */

            //this.Icon = SocializeDesktop.Properties.Resources.ic_launcher_web;
            ContextMenuStrip cms = new ContextMenuStrip();
            cms.ItemClicked += Cms_ItemClicked;
            ToolStripMenuItem tsmi = new ToolStripMenuItem();
            tsmi.Name = "mnuOpen";
            tsmi.Text = "Open";
            cms.Items.Add(tsmi);

            /*
            tsmi = new ToolStripMenuItem();
            tsmi.Name = "mnuSettings";
            tsmi.Text = "Settings";
            cms.Items.Add(tsmi);

            tsmi = new ToolStripMenuItem();
            tsmi.Name = "mnuClose";
            tsmi.Text = "Close";
            cms.Items.Add(tsmi);
            */

            NotifyIcon ni = new NotifyIcon();
            ni.Icon = SocializeDesktop.Properties.Resources.ic_launcher;
            ni.Visible = true;
            ni.ContextMenuStrip = cms;
            RequestClose = false;
            this.ShowInTaskbar = false;
            this.Closing += MainWindow_Closing;


            /* keyboard and mouse events */
            keyboard = new KeyboardInput();
            keyboard.KeyBoardKeyPressed += keyboard_KeyBoardKeyPressed;

            mouse = new MouseInput();
            mouse.MouseMoved += mouse_MouseMoved;

            lastInput = new AllInputSources();

            /*
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            timer.Tick += timer_Tick;
            timer.Start();
            */
            ///signIn();
            
            SystemEvents.PowerModeChanged += OnPowerChange;

        }

        async void OnPowerChange(object s, PowerModeChangedEventArgs e)
        {
            DocumentReference docRef = db.Collection("users").Document(email);
            Dictionary<string, object> input;

            switch (e.Mode)
            {
                case PowerModes.Resume:
                    
                    input = new Dictionary<string, object>
{
                        { "UsingPC", true }
};
                    await docRef.UpdateAsync(input).ConfigureAwait(false);
                    break;
                case PowerModes.Suspend:
                   
                    input = new Dictionary<string, object>
{
                        { "UsingPC", false }
};
                    await docRef.UpdateAsync(input).ConfigureAwait(false);
                    break;
            }
        }

        
        /*
        protected async virtual void OnSessionEnding(System.Windows.SessionEndingCancelEventArgs e)
        {
            DocumentReference docRef = db.Collection("users").Document(email);
            Dictionary<string, object> input;

                    input = new Dictionary<string, object>
{
                        { "UsingPC", false }
};
                    await docRef.UpdateAsync(input).ConfigureAwait(false);
                    
        }*/

        public async void EndSession ()
        {
            endSession = true; 
            DocumentReference docRef = db.Collection("users").Document(email);
            Dictionary<string, object> input;

            input = new Dictionary<string, object>
{
                        { "UsingPC", false }
};
            await docRef.UpdateAsync(input).ConfigureAwait(false);

        }

        public void ResumeSession()
        {
            endSession = false;
          
        }
        /*
        protected async override void OnShutdown()
        {
            //Your code here
            DocumentReference docRef = db.Collection("users").Document(email);
            Dictionary<string, object> input;

            input = new Dictionary<string, object>
{
                        { "UsingPC", false }
};
            await docRef.UpdateAsync(input).ConfigureAwait(false);

            //Don't forget to call ServiceBase OnShutdown()
            base.OnShutdown();
        }

        // When the user presses stops your service from the control panel
        protected override void OnStop()
        {
            // Add your save code here too
            base.OnStop();
        }
        */

        // ref http://stackoverflow.com/a/3978040
        public static int GetRandomUnusedPort()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            // Generates state and PKCE values.
            string state = randomDataBase64url(32);
            string code_verifier = randomDataBase64url(32);
            string code_challenge = base64urlencodeNoPadding(sha256(code_verifier));
            const string code_challenge_method = "S256";

            // Creates a redirect URI using an available port on the loopback address.
            string redirectURI = string.Format("http://{0}:{1}/", IPAddress.Loopback, GetRandomUnusedPort());
            output("redirect URI: " + redirectURI);

            // Creates an HttpListener to listen for requests on that redirect URI.
            var http = new HttpListener();
            http.Prefixes.Add(redirectURI);
            output("Listening..");
            http.Start();

            // Creates the OAuth 2.0 authorization request.
            string authorizationRequest = string.Format("{0}?response_type=code&scope=openid%20email&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}",
                authorizationEndpoint,
                System.Uri.EscapeDataString(redirectURI),
                clientID,
                state,
                code_challenge,
                code_challenge_method);

            // Opens request in the browser.
            System.Diagnostics.Process.Start(authorizationRequest);

            // Waits for the OAuth authorization response.
            var context = await http.GetContextAsync();

            // Brings this app back to the foreground.
            this.Activate();

            // Sends an HTTP response to the browser.
            var response = context.Response;
            string responseString = string.Format("<html><head><meta http-equiv='refresh' content='10;url=https://google.com'></head><body>Please return to the app.</body></html>");
            var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            var responseOutput = response.OutputStream;
            Task responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) =>
            {
                responseOutput.Close();
                http.Stop();
                Console.WriteLine("HTTP server stopped.");
            });

            // Checks for errors.
            if (context.Request.QueryString.Get("error") != null)
            {
                output(String.Format("OAuth authorization error: {0}.", context.Request.QueryString.Get("error")));
                return;
            }
            if (context.Request.QueryString.Get("code") == null
                || context.Request.QueryString.Get("state") == null)
            {
                output("Malformed authorization response. " + context.Request.QueryString);
                return;
            }

            // extracts the code
            var code = context.Request.QueryString.Get("code");
            var incoming_state = context.Request.QueryString.Get("state");

            // Compares the receieved state to the expected value, to ensure that
            // this app made the request which resulted in authorization.
            if (incoming_state != state)
            {
                output(String.Format("Received request with invalid state ({0})", incoming_state));
                return;
            }
            output("Authorization code: " + code);

            // Starts the code exchange at the Token Endpoint.
            performCodeExchange(code, code_verifier, redirectURI);
        }

        async void performCodeExchange(string code, string code_verifier, string redirectURI)
        {
            output("Exchanging code for tokens...");

            // builds the  request
            string tokenRequestURI = "https://www.googleapis.com/oauth2/v4/token";
            string tokenRequestBody = string.Format("code={0}&redirect_uri={1}&client_id={2}&code_verifier={3}&client_secret={4}&scope=&grant_type=authorization_code",
                code,
                System.Uri.EscapeDataString(redirectURI),
                clientID,
                code_verifier,
                clientSecret
                );

            // sends the request
            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(tokenRequestURI);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] _byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = _byteVersion.Length;
            Stream stream = tokenRequest.GetRequestStream();
            await stream.WriteAsync(_byteVersion, 0, _byteVersion.Length);
            stream.Close();

            try
            {
                // gets the response
                WebResponse tokenResponse = await tokenRequest.GetResponseAsync();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = await reader.ReadToEndAsync();
                    output(responseText);

                    // converts to dictionary
                    Dictionary<string, string> tokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);

                    string access_token = tokenEndpointDecoded["access_token"];
                    userinfoCall(access_token);
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        output("HTTP: " + response.StatusCode);
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            string responseText = await reader.ReadToEndAsync();
                            output(responseText);
                        }
                    }

                }
            }
        }

        private async void signIn()
        {
            // Generates state and PKCE values.
            string state = randomDataBase64url(32);
            string code_verifier = randomDataBase64url(32);
            string code_challenge = base64urlencodeNoPadding(sha256(code_verifier));
            const string code_challenge_method = "S256";

            // Creates a redirect URI using an available port on the loopback address.
            string redirectURI = string.Format("http://{0}:{1}/", IPAddress.Loopback, GetRandomUnusedPort());
            output("redirect URI: " + redirectURI);

            // Creates an HttpListener to listen for requests on that redirect URI.
            var http = new HttpListener();
            http.Prefixes.Add(redirectURI);
            output("Listening..");
            http.Start();

            // Creates the OAuth 2.0 authorization request.
            string authorizationRequest = string.Format("{0}?response_type=code&scope=openid%20email&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}",
                authorizationEndpoint,
                System.Uri.EscapeDataString(redirectURI),
                clientID,
                state,
                code_challenge,
                code_challenge_method);

            // Opens request in the browser.
            System.Diagnostics.Process.Start(authorizationRequest);

            // Waits for the OAuth authorization response.
            var context = await http.GetContextAsync();

            // Brings this app back to the foreground.
            this.Activate();

            // Sends an HTTP response to the browser.
            var response = context.Response;
            string responseString = string.Format("<html><head><meta http-equiv='refresh' content='10;url=https://google.com'></head><body>Please return to the app.</body></html>");
            var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            var responseOutput = response.OutputStream;
            Task responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) =>
            {
                responseOutput.Close();
                http.Stop();
                Console.WriteLine("HTTP server stopped.");
            });

            // Checks for errors.
            if (context.Request.QueryString.Get("error") != null)
            {
                output(String.Format("OAuth authorization error: {0}.", context.Request.QueryString.Get("error")));
                return;
            }
            if (context.Request.QueryString.Get("code") == null
                || context.Request.QueryString.Get("state") == null)
            {
                output("Malformed authorization response. " + context.Request.QueryString);
                return;
            }

            // extracts the code
            var code = context.Request.QueryString.Get("code");
            var incoming_state = context.Request.QueryString.Get("state");

            // Compares the receieved state to the expected value, to ensure that
            // this app made the request which resulted in authorization.
            if (incoming_state != state)
            {
                output(String.Format("Received request with invalid state ({0})", incoming_state));
                return;
            }
            output("Authorization code: " + code);

            // Starts the code exchange at the Token Endpoint.
            performCodeExchange(code, code_verifier, redirectURI);
        }



        async void userinfoCall(string access_token)
        {
            output("Making API Call to Userinfo...");

            // builds the  request
            string userinfoRequestURI = "https://www.googleapis.com/oauth2/v3/userinfo";

            // sends the request
            HttpWebRequest userinfoRequest = (HttpWebRequest)WebRequest.Create(userinfoRequestURI);
            userinfoRequest.Method = "GET";
            userinfoRequest.Headers.Add(string.Format("Authorization: Bearer {0}", access_token));
            userinfoRequest.ContentType = "application/x-www-form-urlencoded";
            userinfoRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            // gets the response
            WebResponse userinfoResponse = await userinfoRequest.GetResponseAsync();
            using (StreamReader userinfoResponseReader = new StreamReader(userinfoResponse.GetResponseStream()))
            {
                // reads response body
                string userinfoResponseText = await userinfoResponseReader.ReadToEndAsync();
                output(userinfoResponseText);

                Dictionary<string, string> userResponseDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(userinfoResponseText);
                email = userResponseDecoded["email"];
                textBoxOutput.Text = "Logged in: " + email + ".\n" + "You can close the app, it will help you mitigate your multi-device habits!";
                startLoopInputTime(email);
            }
        }

        /// <summary>
        /// Appends the given string to the on-screen log, and the debug console.
        /// </summary>
        /// <param name="output">string to be appended</param>
        public void output(string output)
        {
            //textBoxOutput.Text = textBoxOutput.Text + output + Environment.NewLine;
            textBoxOutput.Text = "Sign in to Feel your MultiDevice Habits!";
            Console.WriteLine(output);
        }

        /// <summary>
        /// Returns URI-safe data with a given input length.
        /// </summary>
        /// <param name="length">Input length (nb. output will be longer)</param>
        /// <returns></returns>
        public static string randomDataBase64url(uint length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);
            return base64urlencodeNoPadding(bytes);
        }

        /// <summary>
        /// Returns the SHA256 hash of the input string.
        /// </summary>
        /// <param name="inputStirng"></param>
        /// <returns></returns>
        public static byte[] sha256(string inputStirng)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(inputStirng);
            SHA256Managed sha256 = new SHA256Managed();
            return sha256.ComputeHash(bytes);
        }

        /// <summary>
        /// Base64url no-padding encodes the given input buffer.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static string base64urlencodeNoPadding(byte[] buffer)
        {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }


        void keyboard_KeyBoardKeyPressed(object sender, EventArgs e)
        {
            //keyboardTime.Content = FormatDateTime(DateTime.Now);
        }


        private string FormatDateTime(DateTime dateTime)
        {
            return dateTime.ToString("HH:mm:ss fff", CultureInfo.CurrentUICulture);
        }

        
        void mouse_MouseMoved(object sender, EventArgs e)
        {
            //mouseTime.Content = FormatDateTime(DateTime.Now);
        }

        async void timer_Tick(object sender, EventArgs e)
        {
            //lastInputTime.Content = FormatDateTime(lastInput.GetLastInputTime());

            //Timestamp timestampLastInput = Timestamp.FromDateTime(DateTime.SpecifyKind(lastInput.GetLastInputTime(), DateTimeKind.Utc));
    
            //long millisecondsLastInput = DateTime.SpecifyKind(lastInput.GetLastInputTime(), DateTimeKind.Local).Ticks / 10000;
            //long millisecondsNow = DateTime.Now.Ticks / 10000;

            long millisecondsLastInput = new DateTimeOffset(DateTime.SpecifyKind(lastInput.GetLastInputTime(), DateTimeKind.Local)).ToUnixTimeMilliseconds();
            long millisecondsNow = new DateTimeOffset(DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local)).ToUnixTimeMilliseconds();

            /*
            if (email != null && millisecondsNow - millisecondsLastInput >= 120000)
            {
                DocumentReference docRef = db.Collection("users").Document(email);
                Dictionary<string, object> input = new Dictionary<string, object>
{
            { "UsingPC", false }
};
                await docRef.UpdateAsync(input);

            } else */
            if (endSession == false && email != null && millisecondsNow - millisecondsLastInput < 120000 && Math.Abs(millisecondsLastInput - lastSavedInput) > 500)
            {
                DocumentReference docRef = db.Collection("users").Document(email);
                Dictionary<string, object> input = new Dictionary<string, object>
{
            { "UsingPC", true }, {"LastPCInput", millisecondsLastInput }
};
                lastSavedInput = millisecondsLastInput;
                await docRef.UpdateAsync(input);
            }
        }


        async void startLoopInputTime(String email)
        {
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan();
            timer.Interval = TimeSpan.FromSeconds(10);
            timer.Tick += timer_Tick;
            timer.Start();

            // Hook up timer's tick event handler.  
            //this.timer.Tick += new System.EventHandler(this.timer_Tick);

        }
    }
}
